A quick and dirty archive using https://github.com/voussoir/timesearch

Specifically I ran the following commands after setting up timesearch:

```
python timesearch.py get_comments -r redscarepod

python timesearch.py get_submissions -r redscarepod

sqlite3 -header -csv redscarepod.db 'select * from comments;' > redscarepod-comments.csv

sqlite3 -header -csv redscarepod.db 'select * from submissions;' > redscarepod-submissions.csv
```
